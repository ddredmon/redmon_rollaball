﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    private bool braking;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        
    }

    private void Update()
    {
        braking = Input.GetKey(KeyCode.Space);
    }
    void FixedUpdate()
    {
        // move ball
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);

        if (braking)
        {
            float brakeAmount = .85f;
            rb.velocity = new Vector3(rb.velocity.x * brakeAmount, rb.velocity.y, rb.velocity.z * brakeAmount);
        }
    }
  
    void OnTriggerEnter(Collider other)
    {
        // checks object and determines if it can be picked up
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
       
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Bumper"))
        {
            SceneManager.LoadScene("PlayArea");
        }
    }

    void SetCountText ()
    {
        //shows score
        countText.text = "Count: " + count.ToString();
        if (count >= 18)
        {
            winText.text = "You Win!";
        }
    }
}